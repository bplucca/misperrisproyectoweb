from django.db import models

# Create your models here.
class Mascota(models.Model):
    fotografia = models.ImageField(null=True, blank=True, upload_to="media")
    nombre = models.CharField(max_length=25)
    raza = models.CharField(max_length=15)
    desc = models.TextField()
    estado = models.CharField(max_length=15)