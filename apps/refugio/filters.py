import django_filters
from apps.refugio.models import Mascota

class MascotaFilter(django_filters.FilterSet):
    class Meta:
        model = Mascota
        fields = {
            'estado',
        }