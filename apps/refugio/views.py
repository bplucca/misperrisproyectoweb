from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.urls import reverse_lazy

from apps.refugio.models import Mascota
from apps.refugio.forms import MascotaForm

from apps.refugio.filters import MascotaFilter

from .serializers import MascotaSerializers
from django.shortcuts import get_object_or_404
from rest_framework import generics

# Create your views here.

class MascotaListAPI(generics.ListCreateAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializers

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk = self.kwargs['pk'],
        )
        return obj

class MascotasList(ListView):
    model = Mascota
    template_name = 'refugio/list_mascota.html'

def reg_mascota(request):
    if request.method == 'POST':
        form = MascotaForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('done')
    else:
        form = MascotaForm()
    
    return render(request, 'refugio/reg_mascota.html', {'form':form})

def done(request):
    return render(request, 'refugio/done.html')

def edit_mascota(request, id_mascota):
    mascota = Mascota.objects.get(id = id_mascota)
    if request.method == 'GET':
        form = MascotaForm(instance=mascota)
    else:
        form = MascotaForm(request.POST, instance=mascota)
        if form.is_valid():
            form.save()
        return redirect('listar')
    return render(request,'refugio/edit_mascota.html',{'form':form})

def delete_mascota(request, id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'POST':
        mascota.delete()
        return redirect('listar')
    return render(request,'refugio/delete_mascota.html',{'mascota':mascota})

def search(request):
    mascota_list = Mascota.objects.all()
    mascota_filter = MascotaFilter(request.GET, queryset=mascota_list)
    return render(request, 'refugio/list_mascota.html', {'filter':mascota_filter})