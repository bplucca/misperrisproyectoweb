from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('reg_mascota',views.reg_mascota,name='reg_mascota'),
    path('done',views.done,name='done'),
    path('listar/',views.search,name='listar'),
    url(r'^editar/(?P<id_mascota>\d+)/$',views.edit_mascota, name='mascota_editar'),
    url(r'^eliminar/(?P<id_mascota>\d+)/$', views.delete_mascota, name='mascota_eliminar'),
    url(r'^listapi/$', views.MascotaListAPI.as_view(), name='listapi'),
]