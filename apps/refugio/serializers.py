from .models import Mascota
from rest_framework import serializers

class MascotaSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mascota
        fields = ('fotografia','nombre','raza','desc','estado')