# Generated by Django 2.1.3 on 2018-11-13 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('refugio', '0004_auto_20181112_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mascota',
            name='fotografia',
            field=models.ImageField(blank=True, null=True, upload_to='media'),
        ),
    ]
