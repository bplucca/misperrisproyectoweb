from django import forms

from apps.refugio.models import Mascota

class MascotaForm(forms.ModelForm):

    class Meta:
        model = Mascota
        fields = [
            'nombre',
            'raza',
            'desc',
            'fotografia',
            'estado',
        ]