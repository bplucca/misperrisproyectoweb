from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('_registro', views.registro, name='registro'),
    path('', views.index, name='index'),
    path('_register', views.regform, name='_register'),
    path('login',views.signin,name='login'),
]