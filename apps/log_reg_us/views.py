from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login
from apps.log_reg_us.forms import SignUpForm

# Create your views here.
def registro(request):
    return render(request, 'log_reg_us/registro.html')

def index(request):
    return render(request, 'log_reg_us/index.html')

def regform(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')

    else:
        form = UserCreationForm()

    context = {'form' : form}

    return render(request, 'log_reg_us/regform.html', context)

def signin(request):
    if request.method == "POST":
        form = AuthenticationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')

    else:
        form = AuthenticationForm()

    context = {'form' : form}

    return render(request, 'log_reg_us/login.html', context)